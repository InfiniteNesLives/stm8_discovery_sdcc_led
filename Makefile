
#Build directory
BUILD = build

#non-main build dir for non main files placed in dir above
#because main has to be first object file when compiling, need to place it in a separate dir
NONMAIN = nonmain

#include file directory
INCLUDE=-I ./include

#source file directory
#must manually list out all the file names in objects dependency below
SOURCE_DIR=source

#project name
#doesn't need to be associated with any file names
PROJ = led

CC = sdcc


CFLAGS= --std-c99
#CFLAGS+=--opt-code-speed
#CFLAGS+=--opt-code-size
#CFLAGS+=--verbose
#CFLAGS+=--debug 
#CFLAGS+=--stack-auto All functions in the source file will be compiled as reentrant
DEFINE= -mstm8
INCLUDE= -I ./include
CFLAGS+= $(DEFINE) $(INCLUDE) 

PRGMR= stlinkv2
DEVICE= stm8s003?3

all: dir objects
	$(CC) $(OBJECTS) $(CFLAGS) -o $(BUILD)/$(PROJ).ihx

#MUST MANUALLY LIST OUT ALL SOURCE .c FILES BELOW
objects:
	$(CC) -c $(SOURCE_DIR)/main.c 		$(CFLAGS) -o $(BUILD)/
	$(CC) -c $(SOURCE_DIR)/interrupts.c 	$(CFLAGS) -o $(BUILD)/$(NONMAIN)/

#output object files
OBJECTS=$(wildcard $(BUILD)/*.rel $(BUILD)/$(NONMAIN)/*.rel)

program: all
	stm8flash.exe -c $(PRGMR) -p $(DEVICE) -s flash -w $(BUILD)/$(PROJ).ihx

dir:
	mkdir -p $(BUILD)
	mkdir -p $(BUILD)/$(NONMAIN)

clean: 
	rm -rf $(BUILD)
