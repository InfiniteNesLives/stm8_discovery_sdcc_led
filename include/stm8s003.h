#ifndef _stm8s003_h
#define _stm8s003_h
//STM8S003 REGISTER MAP

//	GPIO BLOCKS
//Port A
#define PA_ODR	(*(volatile uint8_t *)0x5000) //data output latch register 0x00
#define PA_IDR	(*(volatile uint8_t *)0x5001) //input pin value register 0xXX(1)
#define PA_DDR	(*(volatile uint8_t *)0x5002) //data direction register 0x00
#define PA_CR1	(*(volatile uint8_t *)0x5003) //control register 1 0x00
#define PA_CR2	(*(volatile uint8_t *)0x5004) //control register 2 0x00
//Port B
#define PB_ODR	(*(volatile uint8_t *)0x5005) //data output latch register 0x00
#define PB_IDR	(*(volatile uint8_t *)0x5006) //input pin value register 0xXX(1)
#define PB_DDR	(*(volatile uint8_t *)0x5007) //data direction register 0x00
#define PB_CR1	(*(volatile uint8_t *)0x5008) //control register 1 0x00
#define PB_CR2	(*(volatile uint8_t *)0x5009) //control register 2 0x00
//Port C
#define PC_ODR	(*(volatile uint8_t *)0x500A) //data output latch register 0x00
#define PC_IDR	(*(volatile uint8_t *)0x500B) //input pin value register 0xXX(1)
#define PC_DDR	(*(volatile uint8_t *)0x500C) //data direction register 0x00
#define PC_CR1	(*(volatile uint8_t *)0x500D) //control register 1 0x00
#define PC_CR2	(*(volatile uint8_t *)0x500E) //control register 2 0x00
//Port D
#define PD_ODR	(*(volatile uint8_t *)0x500F) //data output latch register 0x00
#define PD_IDR	(*(volatile uint8_t *)0x5010) //input pin value register 0xXX(1)
#define PD_DDR	(*(volatile uint8_t *)0x5011) //data direction register 0x00
#define PD_CR1	(*(volatile uint8_t *)0x5012) //control register 1 0x02
#define PD_CR2	(*(volatile uint8_t *)0x5013) //control register 2 0x00
//Port E
#define PE_ODR	(*(volatile uint8_t *)0x5014) //data output latch register 0x00
#define PE_IDR	(*(volatile uint8_t *)0x5015) //input pin value register 0xXX(1)
#define PE_DDR	(*(volatile uint8_t *)0x5016) //data direction register 0x00
#define PE_CR1	(*(volatile uint8_t *)0x5017) //control register 1 0x00
#define PE_CR2	(*(volatile uint8_t *)0x5018) //control register 2 0x00
//Port F
#define PF_ODR	(*(volatile uint8_t *)0x5019) //data output latch register 0x00
#define PF_IDR	(*(volatile uint8_t *)0x501A) //input pin value register 0xXX(1)
#define PF_DDR	(*(volatile uint8_t *)0x501B) //data direction register 0x00
#define PF_CR1	(*(volatile uint8_t *)0x501C) //control register 1 0x00
#define PF_CR2	(*(volatile uint8_t *)0x501D) //control register 2 0x00

//	FLASH BLOCK
//0x00 505A FLASH_CR1 Flash control register 1 0x00
//0x00 505B FLASH_CR2 Flash control register 2 0x00
//0x00 505C FLASH_NCR2 Flash complementary control register 2 0xFF
//0x00 505D FLASH_FPR Flash protection register 0x00
//0x00 505E FLASH_NFPR Flash complementary protection register 0xFF
//0x00 505F FLASH_IAPSR Flash in-application programming status
//0x00 5060 to 0x00 5061 Reserved area (2 byte)
//0x00 5062 FLASH_PUKR Flash Program memory unprotection
//register 0x00 0x00 5063 Reserved area (1 byte)
//0x00 5064 FLASH_DUKR Data EEPROM unprotection register 0x00
//0x00 5065 to 0x00 509F Reserved area (59 byte)

//	ITC BLOCK
//0x00 50A0 EXTI_CR1 External interrupt control register 1 0x00
//0x00 50A1 EXTI_CR2 External interrupt control register 2 0x00
//0x00 50A2 to 0x00 50B2 Reserved area (17 byte)

//	RST BLOCK
//0x00 50B3 RST_SR Reset status register 0xXX(1)
//0x00 50B4 to 0x00 50BF Reserved area (12 byte)

//	CLK BLOCK
//0x00 50C0 CLK CLK_ICKR Internal clock control register 0x01
#define CLK_ICKR	(*(volatile uint8_t *)0x50C0) 
//Bits 7:6 Reserved, must be kept cleared.
#define REGAH	5 //Bit 5 REGAH: Regulator power off in Active-halt mode
//This bit is set and cleared by software. When it is set, the main voltage 
//regulator is powered off as soon as the MCU enters Active-halt mode, 
// so the wakeup time is longer.
//0: MVR regulator ON in Active-halt mode
//1: MVR regulator OFF in Active-halt mode
#define LSIRDY	4 //Bit 4 LSIRDY: Low speed internal oscillator ready
//This bit is set and cleared by hardware.
//0: LSI clock not ready
//1: LSI clock ready
#define LSIEN	3 //Bit 3 LSIEN: Low speed internal RC oscillator enable
//This bit is set and cleared by software. It is set by hardware 
//whenever the LSI oscillator is required,
//    for example:
//    - When switching to the LSI clock (see CLK_SWR register)
//    - When LSI is selected as the active CCO source (see CLK_CCOR register)
//    - When BEEP is enabled (BEEPEN bit set in the BEEP_CSR register)
//    - When LSI measurement is enabled (MSR bit set in the AWU_CSR register)
//	It cannot be cleared when LSI is selected as master clock source 
//	(CLK_CMSR register), as active CCO source or as clock source for 
//	the AWU peripheral or independent Watchdog.
//	0: Low-speed internal RC off
//	1: Low-speed internal RC on
#define FHWU	2 //Bit 2 FHWU: Fast wakeup from Halt/Active-halt modes
//This bit is set and cleared by software.
//0: Fast wakeup from Halt/Active-halt modes disabled
//1: Fast wakeup from Halt/Active-halt modes enabled
#define HSIRDY	1 //Bit 1 HSIRDY: High speed internal oscillator ready
//This bit is set and cleared by hardware.
//0: HSI clock not ready
//1: HSI clock ready
#define HSIEN	0 //Bit 0 HSIEN: High speed internal RC oscillator enable
//This bit is set and cleared by software. It is set by hardware whenever the 
//HSI oscillator is required, for example:
//     - When activated as safe oscillator by the CSS
//     - When switching to HSI clock (see CLK_SWR register)
//     - When HSI is selected as the active CCO source (see CLK_CCOR register)
//	It cannot be cleared when HSI is selected as clock master 
//	(CLK_CMSR register), as active CCO source or if the safe 
//	oscillator (AUX) is enabled.
//	0: High-speed internal RC off
//	1: High-speed internal RC on


//0x00 50C1 CLK_ECKR External clock control register 0x00
//0x00 50C2 Reserved area (1 byte)
//0x00 50C3 CLK_CMSR Clock master status register 0xE1

//0x00 50C4 CLK_SWR Clock master switch register 0xE1
#define CLK_SWR		(*(volatile uint8_t *)0x50C4) 
//0xE1: HSI selected as master clock source (reset value)
#define CLK_SWR_HSI 0xE1
//0xD2: LSI selected as master clock source (only if LSI_EN option bit is set)
#define CLK_SWR_LSI 0xD2
//0xB4: HSE selected as master clock source
#define CLK_SWR_HSE 0xB4

//Clock switch control register 0xXX
#define CLK_SWCR	(*(volatile uint8_t *)0x50C5) 
//Bits 7:4 Reserved.
#define	SWIF	3 //Bit 3 SWIF: Clock switch interrupt flag
//This bit is set by hardware and cleared by software writing 0. 
//Its meaning depends on the status of the SWEN bit. 
//Refer to Figure 22 and Figure 23.
//In manual switching mode (SWEN = 0):
//0: Target clock source not ready
//1: Target clock source ready
//In automatic switching mode (SWEN = 1):
//0: No clock switch event occurred
//1: Clock switch event occurred
#define SWIEN	2 //Bit 2 SWIEN: Clock switch interrupt enable
//This bit is set and cleared by software.
//0: Clock switch interrupt disabled
//1: Clock switch interrupt enabled
#define SWEN	1 //Bit 1 SWEN: Switch start/stop
//This bit is set and cleared by software. 
//Writing a 1 to this bit enables switching the master clock to
//the source defined in the CLK_SWR register.
//0: Disable clock switch execution
//1: Enable clock switch execution
#define SWBSY	0 //Bit 0 SWBSY: Switch busy
//This bit is set and cleared by hardware. 
//It can be cleared by software to reset the clock switch
//process.
//0: No clock switch ongoing
//1: Clock switch ongoing


//Clock divider register 0x18
#define CLK_CKDIVR	(*(volatile uint8_t *)0x50C6) 
//Bits 7:5 Reserved, must be kept cleared.
//Bits 4:3 HSIDIV[1:0]: High speed internal clock prescaler
//These bits are written by software to define the HSI prescaling factor.
//00: fHSI= fHSI RC output
//01: fHSI= fHSI RC output/2
//10: fHSI= fHSI RC output/4
//11: fHSI= fHSI RC output/8
//Bits 2:0 CPUDIV[2:0]: CPU clock prescaler
//These bits are written by software to define the CPU clock prescaling factor.
//000: fCPU=fMASTER
//001: fCPU=fMASTER/2
//010: fCPU=fMASTER/4
//011: fCPU=fMASTER/8
//100: fCPU=fMASTER/16
//101: fCPU=fMASTER/32
//110: fCPU=fMASTER/64
//111: fCPU=fMASTER/128

//Peripheral clock gating register 1 0xFF
#define CLK_PCKENR1	(*(volatile uint8_t *)0x50C7) 

//Clock security system register 0x00
#define CLK_CSSR	(*(volatile uint8_t *)0x50C8) 
//Bits 7:4 Reserved, must be kept cleared.
#define CSSD	3 //Bit 3 CSSD: Clock security system detection
//This bit is set by hardware and cleared by software writing 0.
//0: CSS is off or no HSE crystal clock disturbance detected.
//1: HSE crystal clock disturbance detected.
#define CSSDIE	2 //Bit 2 CSSDIE: Clock security system detection interrupt enable
//This bit is set and cleared by software.
//0: Clock security system interrupt disabled
//1: Clock security system interrupt enabled
#define CSSD_AUX 1 //Bit 1 AUX: Auxiliary oscillator connected to master clock
//This bit is set and cleared by hardware.
//0: Auxiliary oscillator is off.
//1: Auxiliary oscillator (HSI/8) is on and selected as current clock master source.
#define CSSEN	0 //Bit 0 CSSEN: Clock security system enable
//This bit can be read many times and be written once-only by software.
//0: Clock security system off
//1: Clock security system on

//0x00 50C9 CLK_CCOR Configurable clock control register 0x00
//0x00 50CA CLK_PCKENR2 Peripheral clock gating register 2 0xFF
//0x00 50CB Reserved area (1 byte)
//0x00 50CC CLK_HSITRIMR HSI clock calibration trimming register 0x00
//0x00 50CD CLK_SWIMCCR SWIM clock control register 0bXXXX XXX0
//0x00 50CE to 0x00 50D0 Reserved area (3 byte)

//	WWDG BLOCK
//0x00 50D1 WWDG_CR WWDG control register 0x7F
//0x00 50D2 WWDG_WR WWDR window register 0x7F
//0x00 50D3 to 0x00 50DF Reserved area (13 byte)

//	IWDG BLOCK
//0x00 50E0 IWDG_KR IWDG key register 0xXX(2)
//0x00 50E1 IWDG_PR IWDG prescaler register 0x00
//0x00 50E2 IWDG_RLR IWDG reload register 0xFF
//0x00 50E3 to 0x00 50EF Reserved area (13 byte)

//	AWU BLOCK
//0x00 50F0 AWU_CSR1 AWU control/status register 1 0x00
//0x00 50F1 AWU_APR AWU asynchronous prescaler buffer register 0x3F
//0x00 50F2 AWU_TBR AWU timebase selection register 0x00
//0x00 50F3 BEEP BEEP_CSR BEEP control/status register 0x1F
//0x00 50F4 to 0x00 50FF Reserved area (12 byte)

//	SPI BLOCK
//0x00 5200 SPI_CR1 SPI control register 1 0x00
//0x00 5201 SPI_CR2 SPI control register 2 0x00
//0x00 5202 SPI_ICR SPI interrupt control register 0x00
//0x00 5203 SPI_SR SPI status register 0x02
//0x00 5204 SPI_DR SPI data register 0x00
//0x00 5205 SPI_CRCPR SPI CRC polynomial register 0x07
//0x00 5206 SPI_RXCRCR SPI Rx CRC register 0x00
//0x00 5207 SPI_TXCRCR SPI Tx CRC register 0x00
//0x00 5208 to 0x00 520F Reserved area (8 byte)

//	I2C BLOCK
//0x00 5210 I2C_CR1 I2C control register 1 0x00
//0x00 5211 I2C_CR2 I2C control register 2 0x00
//0x00 5212 I2C_FREQR I2C frequency register 0x00
//0x00 5213 I2C_OARL I2C own address register low 0x00
//0x00 5214 I2C_OARH I2C own address register high 0x00
//0x00 5215 Reserved
//0x00 5216 I2C_DR I2C data register 0x00
//0x00 5217 I2C_SR1 I2C status register 1 0x00
//0x00 5218 I2C_SR2 I2C status register 2 0x00
//0x00 5219 I2C_SR3 I2C status register 3 0x00
//0x00 521A I2C_ITR I2C interrupt control register 0x00
//0x00 521B I2C_CCRL I2C clock control register low 0x00
//0x00 521C I2C_CCRH I2C clock control register high 0x00
//0x00 521D I2C_TRISER I2C TRISE register 0x02
//0x00 521E I2C_PECR I2C packet error checking register 0x00
//0x00 521F to 0x00 522F Reserved area (17 byte)

//	UART1 BLOCK
//0x00 5230 UART1_SR UART1 status register 0xC0
//0x00 5231 UART1_DR UART1 data register 0xXX
//0x00 5232 UART1_BRR1 UART1 baud rate register 1 0x00
//0x00 5233 UART1_BRR2 UART1 baud rate register 2 0x00
//0x00 5234 UART1_CR1 UART1 control register 1 0x00
//0x00 5235 UART1_CR2 UART1 control register 2 0x00
//0x00 5236 UART1_CR3 UART1 control register 3 0x00
//0x00 5237 UART1_CR4 UART1 control register 4 0x00
//0x00 5238 UART1_CR5 UART1 control register 5 0x00
//0x00 5239 UART1_GTR UART1 guard time register 0x00
//0x00 523A UART1_PSCR UART1 prescaler register 0x00
//0x00 523B to 0x00523F Reserved area (21 byte)

//	TIM1 BLOCK
#define TIM1_CR1	(*(volatile uint8_t *)0x5250) //control register 1 0x00
//0x00 5251 TIM1_CR2 TIM1 control register 2 0x00
//0x00 5252 TIM1_SMCR TIM1 slave mode control register 0x00
//0x00 5253 TIM1_ETR TIM1 external trigger register 0x00
//0x00 5254 TIM1_IER TIM1 Interrupt enable register 0x00
//0x00 5255 TIM1_SR1 TIM1 status register 1 0x00
//0x00 5256 TIM1_SR2 TIM1 status register 2 0x00
//0x00 5257 TIM1_EGR TIM1 event generation register 0x00
//0x00 5258 TIM1_CCMR1 TIM1 capture/compare mode register 1 0x00
//0x00 5259 TIM1_CCMR2 TIM1 capture/compare mode register 2 0x00
//0x00 525A TIM1_CCMR3 TIM1 capture/compare mode register 3 0x00
//0x00 525B TIM1_CCMR4 TIM1 capture/compare mode register 4 0x00
//0x00 525C TIM1_CCER1 TIM1 capture/compare enable register 1 0x00
//0x00 525D TIM1_CCER2 TIM1 capture/compare enable register 2 0x00
#define TIM1_CNTRH	(*(volatile uint8_t *)0x525E) //counter high 0x00
#define TIM1_CNTRL	(*(volatile uint8_t *)0x525F) //counter low 0x00
#define TIM1_PSCRH	(*(volatile uint8_t *)0x5260) //prescaler register high 0x00
#define TIM1_PSCRL	(*(volatile uint8_t *)0x5261) //prescaler register low 0x00
//0x00 5262 TIM1_ARRH TIM1 auto-reload register high 0xFF
//0x00 5263 TIM1_ARRL TIM1 auto-reload register low 0xFF
//0x00 5264 TIM1_RCR TIM1 repetition counter register 0x00
//0x00 5265 TIM1_CCR1H TIM1 capture/compare register 1 high 0x00
//0x00 5266 TIM1_CCR1L TIM1 capture/compare register 1 low 0x00
//0x00 5267 TIM1_CCR2H TIM1 capture/compare register 2 high 0x00
//0x00 5268 TIM1_CCR2L TIM1 capture/compare register 2 low 0x00
//0x00 5269 TIM1_CCR3H TIM1 capture/compare register 3 high 0x00
//0x00 526A TIM1_CCR3L TIM1 capture/compare register 3 low 0x00
//0x00 526B TIM1_CCR4H TIM1 capture/compare register 4 high 0x00
//0x00 526C TIM1_CCR4L TIM1 capture/compare register 4 low 0x00
//0x00 526D TIM1_BKR TIM1 break register 0x00
//0x00 526E TIM1_DTR TIM1 dead-time register 0x00
//0x00 526F TIM1_OISR TIM1 output idle state register 0x00
//0x00 5270 to 0x00 52FF Reserved area (147 byte)

//	TIM2 BLOCK
//0x00 5300 TIM2_CR1 TIM2 control register 1 0x00
//0x00 5301 Reserved
//0x00 5302 Reserved
//0x00 5303 TIM2_IER TIM2 interrupt enable register 0x00
//0x00 5304 TIM2_SR1 TIM2 status register 1 0x00
//0x00 5305 TIM2_SR2 TIM2 status register 2 0x00
//0x00 5306 TIM2_EGR TIM2 event generation register 0x00
//0x00 5307 TIM2_CCMR1 TIM2 capture/compare mode register 1 0x00
//0x00 5308 TIM2_CCMR2 TIM2 capture/compare mode register 2 0x00
//0x00 5309 TIM2_CCMR3 TIM2 capture/compare mode register 3 0x00
//0x00 530A TIM2_CCER1 TIM2 capture/compare enable register 1 0x00
//0x00 530B TIM2_CCER2 TIM2 capture/compare enable register 2 0x00
//0x00 530C TIM2_CNTRH TIM2 counter high 0x00
//0x00 530D TIM2_CNTRL TIM2 counter low 0x00
//0x00 530E TIM2_PSCR TIM2 prescaler register 0x00
//0x00 530F TIM2_ARRH TIM2 auto-reload register high 0xFF
//0x00 5310 TIM2_ARRL TIM2 auto-reload register low 0xFF
//0x00 5311 TIM2_CCR1H TIM2 capture/compare register 1 high 0x00
//0x00 5312 TIM2_CCR1L TIM2 capture/compare register 1 low 0x00
//0x00 5313 TIM2_CCR2H TIM2 capture/compare reg. 2 high 0x00
//0x00 5314 TIM2_CCR2L TIM2 capture/compare register 2 low 0x00
//0x00 5315 TIM2_CCR3H TIM2 capture/compare register 3 high 0x00
//0x00 5316 TIM2_CCR3L TIM2 capture/compare register 3 low 0x00
//0x00 5317 to 0x00 533F Reserved area (43 byte)

//	TIM4 BLOCK
//0x00 5340 TIM4_CR1 TIM4 control register 1 0x00
//0x00 5341 Reserved
//0x00 5342 Reserved
//0x00 5343 TIM4_IER TIM4 interrupt enable register 0x00
//0x00 5344 TIM4_SR TIM4 status register 0x00
//0x00 5345 TIM4_EGR TIM4 event generation register 0x00
//0x00 5346 TIM4_CNTR TIM4 counter 0x00
//0x00 5347 TIM4_PSCR TIM4 prescaler register 0x00
//0x00 5348 TIM4_ARR TIM4 auto-reload register 0xFF
//0x00 5349 to 0x00 53DF Reserved area (153 byte)

//	ADC1 BLOCK
//0x00 53E0 to 0x00 53F3 ADC_DBxR ADC data buffer registers 0x00
//0x00 53F4 to 0x00 53FF Reserved area (12 byte)
//0x00 5400 ADC_CSR ADC control/status register 0x00
//0x00 5401 ADC_CR1 ADC configuration register 1 0x00
//0x00 5402 ADC_CR2 ADC configuration register 2 0x00
//0x00 5403 ADC_CR3 ADC configuration register 3 0x00
//0x00 5404 ADC_DRH ADC data register high 0xXX
//0x00 5405 ADC_DRL ADC data register low 0xXX
//0x00 5406 ADC_TDRH ADC Schmitt trigger disable register high 0x00
//0x00 5407 ADC_TDRL ADC Schmitt trigger disable register low 0x00
//0x00 5408 ADC_HTRH ADC high threshold register high 0x03
//0x00 5409 ADC_HTRL ADC high threshold register low 0xFF
//0x00 540A ADC_LTRH ADC low threshold register high 0x00
//0x00 540B ADC_LTRL ADC low threshold register low 0x00
//0x00 540C ADC_AWSRH ADC analog watchdog status register high 0x00
//0x00 540D ADC_AWSRL ADC analog watchdog status register low 0x00
//0x00 540E ADC_AWCRH ADC analog watchdog control register high 0x00
//0x00 540F ADC_AWCRL ADC analog watchdog control register low 0x00
//0x00 5410 to 0x00 57FF Reserved area (1008 byte)

//	CPU(1) accessible via debug only
//0x00 7F00 A Accumulator 0x00
//0x00 7F01 PCE Program counter extended 0x00
//0x00 7F02 PCH Program counter high 0x00
//0x00 7F03 PCL Program counter low 0x00
//0x00 7F04 XH X index register high 0x00
//0x00 7F05 XL X index register low 0x00
//0x00 7F06 YH Y index register high 0x00
//0x00 7F07 YL Y index register low 0x00
//0x00 7F08 SPH Stack pointer high 0x03
//0x00 7F09 SPL Stack pointer low 0xFF
//0x00 7F0A CCR Condition code register 0x28
//0x00 7F0B to 0x00 7F5F Reserved area (85 byte)
//0x00 7F60 CPU CFG_GCR Global configuration register 0x00

//	ITC BLOCK
//0x00 7F70 ITC_SPR1 Interrupt software priority register 1 0xFF
//0x00 7F71 ITC_SPR2 Interrupt software priority register 2 0xFF
//0x00 7F72 ITC_SPR3 Interrupt software priority register 3 0xFF
//0x00 7F73 ITC_SPR4 Interrupt software priority register 4 0xFF
//0x00 7F74 ITC_SPR5 Interrupt software priority register 5 0xFF
//0x00 7F75 ITC_SPR6 Interrupt software priority register 6 0xFF
//0x00 7F76 ITC_SPR7 Interrupt software priority register 7 0xFF
//0x00 7F77 ITC_SPR8 Interrupt software priority register 8 0xFF
//0x00 7F78 to 0x00 7F79 Reserved area (2 byte)
//0x00 7F80 SWIM SWIM_CSR SWIM control status register 0x00
//0x00 7F81 to 0x00 7F8F Reserved area (15 byte)

//	DM BLOCK
//0x00 7F90 DM_BK1RE DM breakpoint 1 register extended byte 0xFF
//0x00 7F91 DM_BK1RH DM breakpoint 1 register high byte 0xFF
//0x00 7F92 DM_BK1RL DM breakpoint 1 register low byte 0xFF
//0x00 7F93 DM_BK2RE DM breakpoint 2 register extended byte 0xFF
//0x00 7F94 DM_BK2RH DM breakpoint 2 register high byte 0xFF
//0x00 7F95 DM_BK2RL DM breakpoint 2 register low byte 0xFF
//0x00 7F96 DM_CR1 DM debug module control register 1 0x00
//0x00 7F97 DM_CR2 DM debug module control register 2 0x00
//0x00 7F98 DM_CSR1 DM debug module control/status register 1 0x10
//0x00 7F99 DM_CSR2 DM debug module control/status register 2 0x00
//0x00 7F9A DM_ENFCTR DM enable function register 0xFF
//0x00 7F9B to 0x00 7F9F Reserved area (5 byte)



//==============================================================================
//
//	STM8S003 OPTION BYTES
//
//==============================================================================
/* Option bytes contain configurations for device hardware features 
 * as well as the memory protection of the device. They are stored 
 * in a dedicated block of the memory. Except for the ROP 
 * (read-out protection) byte, each option byte has to be stored 
 * twice, in a regular form (OPTx) and a complemented one (NOPTx) for redundancy.
 *
 * Option bytes can be modified in ICP mode (via SWIM) by accessing 
 * the EEPROM address shown in Table 12: Option bytes below. 
 * Option bytes can also be modified "on the fly" by the application 
 * in IAP mode, except the ROP option that can only be modified in 
 * ICP mode (via SWIM).
 * 
 * Refer to the STM8S Flash programming manual (PM0051) and STM8 
 * SWIM communication protocol and debug module user manual (UM0470) 
 * for information on SWIM programming procedures.
 */

//Read-out protection (ROP)
//0x4800 OPT0 ROP[7:0] 0x00
//ROP[7:0] Memory readout protection (ROP)
//0xAA: Enable readout protection (write access via SWIM protocol)
//Note: Refer to the family reference manual (RM0016) section on
//Flash/EEPROM memory readout protection for details.

//User boot code (UBC)
//0x4801  OPT1 UBC[7:0] 0x00
//0x4802 NOPT1 NUBC[7:0] 0xFF
//UBC[7:0] User boot code area
//0x00: no UBC, no write-protection
//0x01: Pages 0 defined as UBC, memory write-protected
//0x02: Pages 0 to 1 defined as UBC, memory write-protected
//Page 0 and page 1 contain the interrupt vectors.
//...
//0x7F: Pages 0 to 126 defined as UBC, memory write-protected
//Other values: Pages 0 to 127 defined as UBC, memory-write protected.
//Note: Refer to the family reference manual (RM0016) section on
//Flash/EEPROM write protection for more details.

//Alternate function remapping (AFR)
//0x4803  OPT2  AFR7  AFR6  AFR5  AFR4  AFR3  AFR2  AFR1  AFR0 0x00
//0x4804 NOPT2 NAFR7 NAFR6 NAFR5 NAFR4 NAFR3 NAFR2 NAFR1 NAFR0 0xFF
//AFR[7:0]
//Do not use more than one remapping option in the same port. 
//It is forbidden to enable both AFR1 and AFR0
//---------------------------------------------------------------
//STM8S003K3 alternate function remapping bits for 32-pin devices
//---------------------------------------------------------------
//AFR7Alternate function remapping option 7
//Reserved.
//AFR6 Alternate function remapping option 6
//0: AFR6 remapping option inactive: default alternate function(2)
//1: Port D7 alternate function = TIM1_CH4.
//AFR5 Alternate function remapping option 5
//0: AFR5 remapping option inactive: default alternate function(2)
//1: Port D0 alternate function = CLK_CCO.
//AFR[4:2] Alternate function remapping option 4:2
//Reserved.
//AFR1 Alternate function remapping option 1
//0: AFR1 remapping option inactive: default alternate function(2)
//1: Port A3 alternate function = SPI_NSS; port D2 alternate function
//TIM2_CH3
//AFR0 Alternate function remapping option 0
//Reserved.
//---------------------------------------------------------------
//STM8S003F3 alternate function remapping bits for 20-pin devices
//---------------------------------------------------------------
//AFR7Alternate function remapping option 7
//0: AFR7 remapping option inactive: default alternate function(1)
//1: Port C3 alternate function = TIM1_CH1N; port C4 alternate function =
//TIM1_CH2N.
//AFR6 Alternate function remapping option 6
//Reserved.
//AFR5 Alternate function remapping option 5
//Reserved.
//AFR4 Alternate function remapping option 4
//0: AFR4 remapping option inactive: default alternate function(1).
//1: Port B4 alternate function = ADC_ETR; port B5 alternate function =
//TIM1_BKIN.
//AFR3 Alternate function remapping option 3
//0: AFR3 remapping option inactive: default alternate function(1)
//1: Port C3 alternate function = TLI.
//AFR2 Alternate function remapping option 2
//Reserved.
//AFR1 Alternate function remapping option 1 (2)
//0: AFR1 remapping option inactive: default alternate function(1)
//1: Port A3 alternate function = SPI_NSS; port D2 alternate function =
//TIM2_CH3.
//AFR0 Alternate function remapping option 0(2)
//0: AFR0 remapping option inactive: Default alternate functions(1)
//1: Port C5 alternate function = TIM2_CH1; port C6 alternate function =
//TIM1_CH1; port C7 alternate function = TIM1_CH2.


//Misc. option 
//	   bit      7-5        4       3        2        1          0
//0x4805  OPT3 Reserved  HSITRIM  LSI_EN  IWDG_HW  WWDG_HW  WWDG_HALT 0x00
//0x4806 NOPT3 Reserved NHSITRIM NLSI_EN NIWDG_HW NWWDG_HW NWWDG_HALT 0xFF
//HSITRIM: high-speed internal clock trimming register size
//0: 3-bit trimming supported in CLK_HSITRIMR register
//1: 4-bit trimming supported in CLK_HSITRIMR register
//LSI_EN: Low speed internal clock enable
//0: LSI clock is not available as CPU clock source
//1: LSI clock is available as CPU clock source
//IWDG_HW: Independent watchdog
//0: IWDG Independent watchdog activated by software
//1: IWDG Independent watchdog activated by hardware
//WWDG_HW: Window watchdog activation
//0: WWDG window watchdog activated by software
//1: WWDG window watchdog activated by hardware
//WWDG_HALT: Window watchdog reset on halt
//0: No reset generated on halt if WWDG active
//1: Reset generated on halt if WWDG active

//Clock option 
//	   bit      7-4       3         2      1      0
//0x4807  OPT4 Reserved  EXTCLK  CKAWUSEL  PRSC1  PRSC0 0x00
//0x4808 NOPT4 Reserved NEXTCLK NCKAWUSEL NPRSC1 NPRSC0 0xFF
//EXTCLK: External clock selection
//0: External crystal connected to OSCIN/OSCOUT
//1: External clock signal on OSCIN
//CKAWUSEL: Auto wakeup unit/clock
//0: LSI clock source selected for AWU
//1: HSE clock with prescaler selected as clock source for for AWU
//PRSC[1:0] AWU clock prescaler
//0x: 16 MHz to 128 kHz prescaler
//10: 8 MHz to 128 kHz prescaler
//11: 4 MHz to 128 kHz prescaler

//HSE clock startup 
//0x4809  OPT5  HSECNT[7:0] 0x00
//0x480A NOPT5 NHSECNT[7:0] 0xFF
//HSECNT[7:0]: HSE crystal oscillator stabilization time
//This configures the stabilization time.
//0x00: 2048 HSE cycles
//0xB4: 128 HSE cycles
//0xD2: 8 HSE cycles
//0xE1: 0.5 HSE cycles


#endif
