#ifndef _board_h
#define _board_h

//Define board specific items such as i/o locations

//LED is on PD0
#define	LED	0
#define	LED_DDR PD_DDR
#define	LED_ODR PD_ODR
#define	LED_CR1 PD_CR1

//BUTTON is on PB7
#define	BUTTON	7
#define BUTTON_IDR	PB_IDR

#endif
