#ifndef _interrupts_h
#define _interrupts_h
#include <stdint.h>
#include "stm8s003.h"

/* $8000 -2 RESET vector */
//handled by compiler
/* $8004 -1 TRAP Software interrupt */
//can't figure out how to instantiate this in SDCC ends up pointing to reset vector

void isr_TLI(void) 	__interrupt (0);	
void isr_AWU(void) 	__interrupt (1); 	
void isr_CLK(void) 	__interrupt (2); 	
void isr_EXTI0(void) 	__interrupt (3); 	
void isr_EXTI1(void) 	__interrupt (4); 	
void isr_EXTI2(void) 	__interrupt (5); 	
void isr_EXTI3(void) 	__interrupt (6); 	
void isr_EXTI4(void) 	__interrupt (7); 	
//void isr_reserved8(void) __interrupt (8) /* $8028 */ { }
//void isr_reserved9(void) __interrupt (9) /* $802C */ { }
void isr_SPI(void) 		__interrupt (10); 
void isr_TIM1_update(void)	__interrupt (11); 
void isr_TIM1_capture(void)	__interrupt (12); 
void isr_TIM2_update(void) 	__interrupt (13); 
void isr_TIM2_capture(void) 	__interrupt (14); 
//void isr_reserved15(void) __interrupt (15) /* $8044 */ { }
//void isr_reserved16(void) __interrupt (16) /* $8048 */ { }
void isr_UART1_tx(void)	__interrupt (17);
void isr_UART1_rx(void)	__interrupt (18);	
void isr_I2C(void) 	__interrupt (19);
//void isr_reserved20(void) __interrupt (20) /* $8058 */ { }
//void isr_reserved21(void) __interrupt (21) /* $805C */ { }
void isr_ADC1(void) 	__interrupt (22); 	
void isr_TIM4(void) 	__interrupt (23); 	
void isr_FLASH(void) 	__interrupt (24); 	
//void isr_reserved25(void) __interrupt (25) /* $806C */ { }
//void isr_reserved26(void) __interrupt (26) /* $8070 */ { }
//void isr_reserved27(void) __interrupt (27) /* $8074 */ { }
//void isr_reserved28(void) __interrupt (28) /* $8078 */ { }
//void isr_reserved29(void) __interrupt (29) /* $807C */ { }

#endif
