// Source code under CC0 1.0
#include <stdint.h>
#include "stm8s003.h"
#include "board.h"
#include "interrupts.h"


void init_sys_clk(void)
{
	//At startup the master clock source is automatically 
	//selected as HSI RC clock output divided by 8 (fHSI/8) = 2Mhz
	//set to 16Mhz, then we'll be able to detect if 4Mhz ext clock failed
	//and it switched back to internal 16Mhz
	CLK_CKDIVR = 0;
	
	//First enable the clock security register if using external clock
	//If enabled the mcu will go back to HSI clk in event of HSE failure
	CLK_CSSR = (1<<CSSEN);
	//In testing is appears the CLK_CKDIVR gets set back to default 2Mhz
	//speed when clock security switches back to HSI on HSE failure

	//EXTCLK: External clock selection
	//0: External crystal connected to OSCIN/OSCOUT
	//1: External clock signal on OSCIN
	//EXTCLK option bit this can be programmed in application but it's
	//more difficult than via SWIM ICP for now...
	//must use FLASH_DUKR register to write option bytes (eeprom)

	//perform automatic clock switching to external clock
	//1.Enable the switching mechanism by setting the SWEN bit in the Switch control register (CLK_SWCR).
	CLK_SWCR |= (1<<SWEN);
	//2.Write the 8-bit value used to select the target clock source in the Clock master switch register (CLK_SWR). 
	CLK_SWR = CLK_SWR_HSE;
	//  The SWBSY bit in the CLK_SWCR register is set by hardware, and the target source oscillator starts. 
	//  The old clock source continues to drive the CPU and peripherals.
	//  As soon as the target clock source is ready (stabilized), the content of the CLK_SWR
	//  register is copied to the Clock master status register (CLK_CMSR).
	//  The SWBSY bit is cleared and the new clock source replaces the old one. The SWIF flag in
//	while ((CLK_SWCR & (1<<SWIF)) == 0) { }
	//spin while clock is switching
	//could be doing other things as this isn't really necessary
	//The mcu will hang here if ext clock wasn't present..
	//  the CLK_SWCR is set and an interrupt is generated if the SWIEN bit is set.
	//Now that we've switched over to external clock, we can clear SWIF
	CLK_SWCR &= ~(1<<SWIF);
	//and disable the HSI to save power it'll reenable itself is HSE fails
	//since we enabled the CSS
	CLK_SWR &= ~(1<<HSIEN);
	

}

void init_io(void)
{
	//Most i/o are input floating after reset
	//SWIM i/o is exception having pullup enabled
	//all i/o are setup as slow drive, interrupt disabled
	//However unused i/o should not be left in this setting
	//to avoid excess current.  Should be set to o/p or enable
	//a external or internal pull-up/down
	
	//So let's just enable pullups on all i/o
	PA_CR1 = 0xFF;
	PB_CR1 = 0xFF;
	PC_CR1 = 0xFF;
	PD_CR1 = 0xFF;
	PE_CR1 = 0xFF;
	PF_CR1 = 0xFF;

	//for discovery board enable LED as output
	LED_DDR |= (1<<LED);
	LED_CR1 |= (1<<LED);
}

unsigned int clock(void)
{
	unsigned char h = TIM1_CNTRH;
	unsigned char l = TIM1_CNTRL;
	return((unsigned int)(h) << 8 | l);
}

void main(void)
{
	//Setup clock source
	init_sys_clk();
	
	//Setup i/o
	init_io();


	// Configure timer
	// 1000 ticks per second
	TIM1_PSCRH = 0x3e;
	TIM1_PSCRL = 0x80;
	// Enable timer
	TIM1_CR1 = 0x01;


	for(;;)
	{

		//Blink LED if button not pressed, hold if pressed
		if (BUTTON_IDR &= (1<<BUTTON)) {
			//button not pressed blink
			LED_ODR = clock() % 1000 < 500;
		} else {
			//button pressed hold current LED state
		}

		//simple LED on/off depending on button
		/*
		if (BUTTON_IDR &= (1<<BUTTON)) {
			//button not pressed LED ON
			LED_ODR &= ~(1<<LED);
		} else {
			//button pressed LED OFF
			LED_ODR |= (1<<LED);
		}
		*/
	}
}

