
#include "interrupts.h"

/* $8000 -2 RESET vector */
//handled by compiler
/* $8004 -1 TRAP Software interrupt */
//can't figure out how to instantiate this in SDCC ends up pointing to reset vector

void isr_TLI(void) 	__interrupt (0)		/* $8008 0 TLI External top level interrupt */	{ }
void isr_AWU(void) 	__interrupt (1) 	/* $800C 1 AWU Auto wake up from halt */	{ }
void isr_CLK(void) 	__interrupt (2) 	/* $8010 2 CLK Clock controller */		{ }
void isr_EXTI0(void) 	__interrupt (3) 	/* $8014 3 EXTI0 Port A external interrupts */	{ }
void isr_EXTI1(void) 	__interrupt (4) 	/* $8018 4 EXTI1 Port B external interrupts */	{ }
void isr_EXTI2(void) 	__interrupt (5) 	/* $801C 5 EXTI2 Port C external interrupts */	{ }
void isr_EXTI3(void) 	__interrupt (6) 	/* $8020 6 EXTI3 Port D external interrupts */	{ }
void isr_EXTI4(void) 	__interrupt (7) 	/* $8024 7 EXTI4 Port E external interrupts */	{ }
//void isr_reserved8(void) __interrupt (8) /* $8028 */ { }
//void isr_reserved9(void) __interrupt (9) /* $802C */ { }
void isr_SPI(void) 		__interrupt (10) /* $8030 10 SPI End of transfer */ 		{ }
void isr_TIM1_update(void)	__interrupt (11) /* $8034 11 TIM1 TIM1 update/overflow/underflow/trigger/break */ { }
void isr_TIM1_capture(void)	__interrupt (12) /* $8038 12 TIM1 TIM1 capture/compare */ 	{ }
void isr_TIM2_update(void) 	__interrupt (13) /* $803C 13 TIM2 TIM2 update/overflow */ 	{ }
void isr_TIM2_capture(void) 	__interrupt (14) /* $8040 14 TIM2 TIM2 capture/compare */ 	{ }
//void isr_reserved15(void) __interrupt (15) /* $8044 */ { }
//void isr_reserved16(void) __interrupt (16) /* $8048 */ { }
void isr_UART1_tx(void)	__interrupt (17) 	/* $804C 17 UART1 Tx complete */ 		{ }
void isr_UART1_rx(void)	__interrupt (18) 	/* $8050 18 UART1 Receive register DATA FULL */ { }
void isr_I2C(void) 	__interrupt (19) 	/* $8054 19 I2C I2C interrupt */ 		{ }
//void isr_reserved20(void) __interrupt (20) /* $8058 */ { }
//void isr_reserved21(void) __interrupt (21) /* $805C */ { }
void isr_ADC1(void) 	__interrupt (22) 	/* $8060 22 ADC1 end of conversion/analog watchdog interrupt */ { }
void isr_TIM4(void) 	__interrupt (23) 	/* $8064 23 TIM4 update/overflow */ 		{ }
void isr_FLASH(void) 	__interrupt (24) 	/* $8068 24 Flash EOP/WR_PG_DIS */ 		{ }
//void isr_reserved25(void) __interrupt (25) /* $806C */ { }
//void isr_reserved26(void) __interrupt (26) /* $8070 */ { }
//void isr_reserved27(void) __interrupt (27) /* $8074 */ { }
//void isr_reserved28(void) __interrupt (28) /* $8078 */ { }
//void isr_reserved29(void) __interrupt (29) /* $807C */ { }
