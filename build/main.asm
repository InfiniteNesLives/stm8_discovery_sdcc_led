;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.5 #9860 (MINGW64)
;--------------------------------------------------------
	.module main
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _clock
	.globl _init_io
	.globl _init_sys_clk
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME
__interrupt_vect:
	int s_GSINIT ; reset
	int 0x0000 ; trap
	int _isr_TLI ; int0
	int _isr_AWU ; int1
	int _isr_CLK ; int2
	int _isr_EXTI0 ; int3
	int _isr_EXTI1 ; int4
	int _isr_EXTI2 ; int5
	int _isr_EXTI3 ; int6
	int _isr_EXTI4 ; int7
	int 0x0000 ; int8
	int 0x0000 ; int9
	int _isr_SPI ; int10
	int _isr_TIM1_update ; int11
	int _isr_TIM1_capture ; int12
	int _isr_TIM2_update ; int13
	int _isr_TIM2_capture ; int14
	int 0x0000 ; int15
	int 0x0000 ; int16
	int _isr_UART1_tx ; int17
	int _isr_UART1_rx ; int18
	int _isr_I2C ; int19
	int 0x0000 ; int20
	int 0x0000 ; int21
	int _isr_ADC1 ; int22
	int _isr_TIM4 ; int23
	int _isr_FLASH ; int24
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
__sdcc_gs_init_startup:
__sdcc_init_data:
; stm8_genXINIT() start
	ldw x, #l_DATA
	jreq	00002$
00001$:
	clr (s_DATA - 1, x)
	decw x
	jrne	00001$
00002$:
	ldw	x, #l_INITIALIZER
	jreq	00004$
00003$:
	ld	a, (s_INITIALIZER - 1, x)
	ld	(s_INITIALIZED - 1, x), a
	decw	x
	jrne	00003$
00004$:
; stm8_genXINIT() end
	.area GSFINAL
	jp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
__sdcc_program_startup:
	jp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
;	source/main.c: 8: void init_sys_clk(void)
;	-----------------------------------------
;	 function init_sys_clk
;	-----------------------------------------
_init_sys_clk:
;	source/main.c: 14: CLK_CKDIVR = 0;
	mov	0x50c6+0, #0x00
;	source/main.c: 18: CLK_CSSR = (1<<CSSEN);
	mov	0x50c8+0, #0x01
;	source/main.c: 31: CLK_SWCR |= (1<<SWEN);
	ldw	x, #0x50c5
	ld	a, (x)
	or	a, #0x02
	ld	(x), a
;	source/main.c: 33: CLK_SWR = CLK_SWR_HSE;
	mov	0x50c4+0, #0xb4
;	source/main.c: 45: CLK_SWCR &= ~(1<<SWIF);
	ldw	x, #0x50c5
	ld	a, (x)
	and	a, #0xf7
	ld	(x), a
;	source/main.c: 48: CLK_SWR &= ~(1<<HSIEN);
	bres	0x50c4, #0
	ret
;	source/main.c: 53: void init_io(void)
;	-----------------------------------------
;	 function init_io
;	-----------------------------------------
_init_io:
;	source/main.c: 63: PA_CR1 = 0xFF;
	mov	0x5003+0, #0xff
;	source/main.c: 64: PB_CR1 = 0xFF;
	mov	0x5008+0, #0xff
;	source/main.c: 65: PC_CR1 = 0xFF;
	mov	0x500d+0, #0xff
;	source/main.c: 66: PD_CR1 = 0xFF;
	mov	0x5012+0, #0xff
;	source/main.c: 67: PE_CR1 = 0xFF;
	mov	0x5017+0, #0xff
;	source/main.c: 68: PF_CR1 = 0xFF;
	mov	0x501c+0, #0xff
;	source/main.c: 71: LED_DDR |= (1<<LED);
	bset	0x5011, #0
;	source/main.c: 72: LED_CR1 |= (1<<LED);
	bset	0x5012, #0
	ret
;	source/main.c: 75: unsigned int clock(void)
;	-----------------------------------------
;	 function clock
;	-----------------------------------------
_clock:
	sub	sp, #6
;	source/main.c: 77: unsigned char h = TIM1_CNTRH;
	ldw	x, #0x525e
	ld	a, (x)
	ld	xh, a
;	source/main.c: 78: unsigned char l = TIM1_CNTRL;
	ldw	y, #0x525f
	ld	a, (y)
;	source/main.c: 79: return((unsigned int)(h) << 8 | l);
	clr	(0x01, sp)
	clr	(0x04, sp)
	clr	(0x05, sp)
	or	a, (0x04, sp)
	rlwa	x
	or	a, (0x05, sp)
	ld	xh, a
	addw	sp, #6
	ret
;	source/main.c: 82: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	source/main.c: 85: init_sys_clk();
	call	_init_sys_clk
;	source/main.c: 88: init_io();
	call	_init_io
;	source/main.c: 93: TIM1_PSCRH = 0x3e;
	mov	0x5260+0, #0x3e
;	source/main.c: 94: TIM1_PSCRL = 0x80;
	mov	0x5261+0, #0x80
;	source/main.c: 96: TIM1_CR1 = 0x01;
	mov	0x5250+0, #0x01
00104$:
;	source/main.c: 103: if (BUTTON_IDR &= (1<<BUTTON)) {
	ldw	x, #0x5006
	ld	a, (x)
	and	a, #0x80
	ld	(x), a
	tnz	a
	jreq	00104$
;	source/main.c: 105: LED_ODR = clock() % 1000 < 500;
	call	_clock
	ldw	y, #0x03e8
	divw	x, y
	cpw	y, #0x01f4
	clr	a
	rlc	a
	ldw	x, #0x500f
	ld	(x), a
	jra	00104$
	ret
	.area CODE
	.area INITIALIZER
	.area CABS (ABS)
