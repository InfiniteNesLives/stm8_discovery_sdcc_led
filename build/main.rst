                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.6.5 #9860 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _clock
                                     13 	.globl _init_io
                                     14 	.globl _init_sys_clk
                                     15 ;--------------------------------------------------------
                                     16 ; ram data
                                     17 ;--------------------------------------------------------
                                     18 	.area DATA
                                     19 ;--------------------------------------------------------
                                     20 ; ram data
                                     21 ;--------------------------------------------------------
                                     22 	.area INITIALIZED
                                     23 ;--------------------------------------------------------
                                     24 ; Stack segment in internal ram 
                                     25 ;--------------------------------------------------------
                                     26 	.area	SSEG
      000001                         27 __start__stack:
      000001                         28 	.ds	1
                                     29 
                                     30 ;--------------------------------------------------------
                                     31 ; absolute external ram data
                                     32 ;--------------------------------------------------------
                                     33 	.area DABS (ABS)
                                     34 ;--------------------------------------------------------
                                     35 ; interrupt vector 
                                     36 ;--------------------------------------------------------
                                     37 	.area HOME
      008000                         38 __interrupt_vect:
      008000 82 00 80 6F             39 	int s_GSINIT ; reset
      008004 82 00 00 00             40 	int 0x0000 ; trap
      008008 82 00 81 19             41 	int _isr_TLI ; int0
      00800C 82 00 81 1A             42 	int _isr_AWU ; int1
      008010 82 00 81 1B             43 	int _isr_CLK ; int2
      008014 82 00 81 1C             44 	int _isr_EXTI0 ; int3
      008018 82 00 81 1D             45 	int _isr_EXTI1 ; int4
      00801C 82 00 81 1E             46 	int _isr_EXTI2 ; int5
      008020 82 00 81 1F             47 	int _isr_EXTI3 ; int6
      008024 82 00 81 20             48 	int _isr_EXTI4 ; int7
      008028 82 00 00 00             49 	int 0x0000 ; int8
      00802C 82 00 00 00             50 	int 0x0000 ; int9
      008030 82 00 81 21             51 	int _isr_SPI ; int10
      008034 82 00 81 22             52 	int _isr_TIM1_update ; int11
      008038 82 00 81 23             53 	int _isr_TIM1_capture ; int12
      00803C 82 00 81 24             54 	int _isr_TIM2_update ; int13
      008040 82 00 81 25             55 	int _isr_TIM2_capture ; int14
      008044 82 00 00 00             56 	int 0x0000 ; int15
      008048 82 00 00 00             57 	int 0x0000 ; int16
      00804C 82 00 81 26             58 	int _isr_UART1_tx ; int17
      008050 82 00 81 27             59 	int _isr_UART1_rx ; int18
      008054 82 00 81 28             60 	int _isr_I2C ; int19
      008058 82 00 00 00             61 	int 0x0000 ; int20
      00805C 82 00 00 00             62 	int 0x0000 ; int21
      008060 82 00 81 29             63 	int _isr_ADC1 ; int22
      008064 82 00 81 2A             64 	int _isr_TIM4 ; int23
      008068 82 00 81 2B             65 	int _isr_FLASH ; int24
                                     66 ;--------------------------------------------------------
                                     67 ; global & static initialisations
                                     68 ;--------------------------------------------------------
                                     69 	.area HOME
                                     70 	.area GSINIT
                                     71 	.area GSFINAL
                                     72 	.area GSINIT
      00806F                         73 __sdcc_gs_init_startup:
      00806F                         74 __sdcc_init_data:
                                     75 ; stm8_genXINIT() start
      00806F AE 00 00         [ 2]   76 	ldw x, #l_DATA
      008072 27 07            [ 1]   77 	jreq	00002$
      008074                         78 00001$:
      008074 72 4F 00 00      [ 1]   79 	clr (s_DATA - 1, x)
      008078 5A               [ 2]   80 	decw x
      008079 26 F9            [ 1]   81 	jrne	00001$
      00807B                         82 00002$:
      00807B AE 00 00         [ 2]   83 	ldw	x, #l_INITIALIZER
      00807E 27 09            [ 1]   84 	jreq	00004$
      008080                         85 00003$:
      008080 D6 81 2B         [ 1]   86 	ld	a, (s_INITIALIZER - 1, x)
      008083 D7 00 00         [ 1]   87 	ld	(s_INITIALIZED - 1, x), a
      008086 5A               [ 2]   88 	decw	x
      008087 26 F7            [ 1]   89 	jrne	00003$
      008089                         90 00004$:
                                     91 ; stm8_genXINIT() end
                                     92 	.area GSFINAL
      008089 CC 80 6C         [ 2]   93 	jp	__sdcc_program_startup
                                     94 ;--------------------------------------------------------
                                     95 ; Home
                                     96 ;--------------------------------------------------------
                                     97 	.area HOME
                                     98 	.area HOME
      00806C                         99 __sdcc_program_startup:
      00806C CC 80 E8         [ 2]  100 	jp	_main
                                    101 ;	return from main will return to caller
                                    102 ;--------------------------------------------------------
                                    103 ; code
                                    104 ;--------------------------------------------------------
                                    105 	.area CODE
                                    106 ;	source/main.c: 8: void init_sys_clk(void)
                                    107 ;	-----------------------------------------
                                    108 ;	 function init_sys_clk
                                    109 ;	-----------------------------------------
      00808C                        110 _init_sys_clk:
                                    111 ;	source/main.c: 14: CLK_CKDIVR = 0;
      00808C 35 00 50 C6      [ 1]  112 	mov	0x50c6+0, #0x00
                                    113 ;	source/main.c: 18: CLK_CSSR = (1<<CSSEN);
      008090 35 01 50 C8      [ 1]  114 	mov	0x50c8+0, #0x01
                                    115 ;	source/main.c: 31: CLK_SWCR |= (1<<SWEN);
      008094 AE 50 C5         [ 2]  116 	ldw	x, #0x50c5
      008097 F6               [ 1]  117 	ld	a, (x)
      008098 AA 02            [ 1]  118 	or	a, #0x02
      00809A F7               [ 1]  119 	ld	(x), a
                                    120 ;	source/main.c: 33: CLK_SWR = CLK_SWR_HSE;
      00809B 35 B4 50 C4      [ 1]  121 	mov	0x50c4+0, #0xb4
                                    122 ;	source/main.c: 45: CLK_SWCR &= ~(1<<SWIF);
      00809F AE 50 C5         [ 2]  123 	ldw	x, #0x50c5
      0080A2 F6               [ 1]  124 	ld	a, (x)
      0080A3 A4 F7            [ 1]  125 	and	a, #0xf7
      0080A5 F7               [ 1]  126 	ld	(x), a
                                    127 ;	source/main.c: 48: CLK_SWR &= ~(1<<HSIEN);
      0080A6 72 11 50 C4      [ 1]  128 	bres	0x50c4, #0
      0080AA 81               [ 4]  129 	ret
                                    130 ;	source/main.c: 53: void init_io(void)
                                    131 ;	-----------------------------------------
                                    132 ;	 function init_io
                                    133 ;	-----------------------------------------
      0080AB                        134 _init_io:
                                    135 ;	source/main.c: 63: PA_CR1 = 0xFF;
      0080AB 35 FF 50 03      [ 1]  136 	mov	0x5003+0, #0xff
                                    137 ;	source/main.c: 64: PB_CR1 = 0xFF;
      0080AF 35 FF 50 08      [ 1]  138 	mov	0x5008+0, #0xff
                                    139 ;	source/main.c: 65: PC_CR1 = 0xFF;
      0080B3 35 FF 50 0D      [ 1]  140 	mov	0x500d+0, #0xff
                                    141 ;	source/main.c: 66: PD_CR1 = 0xFF;
      0080B7 35 FF 50 12      [ 1]  142 	mov	0x5012+0, #0xff
                                    143 ;	source/main.c: 67: PE_CR1 = 0xFF;
      0080BB 35 FF 50 17      [ 1]  144 	mov	0x5017+0, #0xff
                                    145 ;	source/main.c: 68: PF_CR1 = 0xFF;
      0080BF 35 FF 50 1C      [ 1]  146 	mov	0x501c+0, #0xff
                                    147 ;	source/main.c: 71: LED_DDR |= (1<<LED);
      0080C3 72 10 50 11      [ 1]  148 	bset	0x5011, #0
                                    149 ;	source/main.c: 72: LED_CR1 |= (1<<LED);
      0080C7 72 10 50 12      [ 1]  150 	bset	0x5012, #0
      0080CB 81               [ 4]  151 	ret
                                    152 ;	source/main.c: 75: unsigned int clock(void)
                                    153 ;	-----------------------------------------
                                    154 ;	 function clock
                                    155 ;	-----------------------------------------
      0080CC                        156 _clock:
      0080CC 52 06            [ 2]  157 	sub	sp, #6
                                    158 ;	source/main.c: 77: unsigned char h = TIM1_CNTRH;
      0080CE AE 52 5E         [ 2]  159 	ldw	x, #0x525e
      0080D1 F6               [ 1]  160 	ld	a, (x)
      0080D2 95               [ 1]  161 	ld	xh, a
                                    162 ;	source/main.c: 78: unsigned char l = TIM1_CNTRL;
      0080D3 90 AE 52 5F      [ 2]  163 	ldw	y, #0x525f
      0080D7 90 F6            [ 1]  164 	ld	a, (y)
                                    165 ;	source/main.c: 79: return((unsigned int)(h) << 8 | l);
      0080D9 0F 01            [ 1]  166 	clr	(0x01, sp)
      0080DB 0F 04            [ 1]  167 	clr	(0x04, sp)
      0080DD 0F 05            [ 1]  168 	clr	(0x05, sp)
      0080DF 1A 04            [ 1]  169 	or	a, (0x04, sp)
      0080E1 02               [ 1]  170 	rlwa	x
      0080E2 1A 05            [ 1]  171 	or	a, (0x05, sp)
      0080E4 95               [ 1]  172 	ld	xh, a
      0080E5 5B 06            [ 2]  173 	addw	sp, #6
      0080E7 81               [ 4]  174 	ret
                                    175 ;	source/main.c: 82: void main(void)
                                    176 ;	-----------------------------------------
                                    177 ;	 function main
                                    178 ;	-----------------------------------------
      0080E8                        179 _main:
                                    180 ;	source/main.c: 85: init_sys_clk();
      0080E8 CD 80 8C         [ 4]  181 	call	_init_sys_clk
                                    182 ;	source/main.c: 88: init_io();
      0080EB CD 80 AB         [ 4]  183 	call	_init_io
                                    184 ;	source/main.c: 93: TIM1_PSCRH = 0x3e;
      0080EE 35 3E 52 60      [ 1]  185 	mov	0x5260+0, #0x3e
                                    186 ;	source/main.c: 94: TIM1_PSCRL = 0x80;
      0080F2 35 80 52 61      [ 1]  187 	mov	0x5261+0, #0x80
                                    188 ;	source/main.c: 96: TIM1_CR1 = 0x01;
      0080F6 35 01 52 50      [ 1]  189 	mov	0x5250+0, #0x01
      0080FA                        190 00104$:
                                    191 ;	source/main.c: 103: if (BUTTON_IDR &= (1<<BUTTON)) {
      0080FA AE 50 06         [ 2]  192 	ldw	x, #0x5006
      0080FD F6               [ 1]  193 	ld	a, (x)
      0080FE A4 80            [ 1]  194 	and	a, #0x80
      008100 F7               [ 1]  195 	ld	(x), a
      008101 4D               [ 1]  196 	tnz	a
      008102 27 F6            [ 1]  197 	jreq	00104$
                                    198 ;	source/main.c: 105: LED_ODR = clock() % 1000 < 500;
      008104 CD 80 CC         [ 4]  199 	call	_clock
      008107 90 AE 03 E8      [ 2]  200 	ldw	y, #0x03e8
      00810B 65               [ 2]  201 	divw	x, y
      00810C 90 A3 01 F4      [ 2]  202 	cpw	y, #0x01f4
      008110 4F               [ 1]  203 	clr	a
      008111 49               [ 1]  204 	rlc	a
      008112 AE 50 0F         [ 2]  205 	ldw	x, #0x500f
      008115 F7               [ 1]  206 	ld	(x), a
      008116 20 E2            [ 2]  207 	jra	00104$
      008118 81               [ 4]  208 	ret
                                    209 	.area CODE
                                    210 	.area INITIALIZER
                                    211 	.area CABS (ABS)
