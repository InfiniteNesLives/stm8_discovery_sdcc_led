;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.5 #9860 (MINGW64)
;--------------------------------------------------------
	.module interrupts
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _isr_TLI
	.globl _isr_AWU
	.globl _isr_CLK
	.globl _isr_EXTI0
	.globl _isr_EXTI1
	.globl _isr_EXTI2
	.globl _isr_EXTI3
	.globl _isr_EXTI4
	.globl _isr_SPI
	.globl _isr_TIM1_update
	.globl _isr_TIM1_capture
	.globl _isr_TIM2_update
	.globl _isr_TIM2_capture
	.globl _isr_UART1_tx
	.globl _isr_UART1_rx
	.globl _isr_I2C
	.globl _isr_ADC1
	.globl _isr_TIM4
	.globl _isr_FLASH
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
;	source/interrupts.c: 9: void isr_TLI(void) 	__interrupt (0)		/* $8008 0 TLI External top level interrupt */	{ }
;	-----------------------------------------
;	 function isr_TLI
;	-----------------------------------------
_isr_TLI:
	iret
;	source/interrupts.c: 10: void isr_AWU(void) 	__interrupt (1) 	/* $800C 1 AWU Auto wake up from halt */	{ }
;	-----------------------------------------
;	 function isr_AWU
;	-----------------------------------------
_isr_AWU:
	iret
;	source/interrupts.c: 11: void isr_CLK(void) 	__interrupt (2) 	/* $8010 2 CLK Clock controller */		{ }
;	-----------------------------------------
;	 function isr_CLK
;	-----------------------------------------
_isr_CLK:
	iret
;	source/interrupts.c: 12: void isr_EXTI0(void) 	__interrupt (3) 	/* $8014 3 EXTI0 Port A external interrupts */	{ }
;	-----------------------------------------
;	 function isr_EXTI0
;	-----------------------------------------
_isr_EXTI0:
	iret
;	source/interrupts.c: 13: void isr_EXTI1(void) 	__interrupt (4) 	/* $8018 4 EXTI1 Port B external interrupts */	{ }
;	-----------------------------------------
;	 function isr_EXTI1
;	-----------------------------------------
_isr_EXTI1:
	iret
;	source/interrupts.c: 14: void isr_EXTI2(void) 	__interrupt (5) 	/* $801C 5 EXTI2 Port C external interrupts */	{ }
;	-----------------------------------------
;	 function isr_EXTI2
;	-----------------------------------------
_isr_EXTI2:
	iret
;	source/interrupts.c: 15: void isr_EXTI3(void) 	__interrupt (6) 	/* $8020 6 EXTI3 Port D external interrupts */	{ }
;	-----------------------------------------
;	 function isr_EXTI3
;	-----------------------------------------
_isr_EXTI3:
	iret
;	source/interrupts.c: 16: void isr_EXTI4(void) 	__interrupt (7) 	/* $8024 7 EXTI4 Port E external interrupts */	{ }
;	-----------------------------------------
;	 function isr_EXTI4
;	-----------------------------------------
_isr_EXTI4:
	iret
;	source/interrupts.c: 19: void isr_SPI(void) 		__interrupt (10) /* $8030 10 SPI End of transfer */ 		{ }
;	-----------------------------------------
;	 function isr_SPI
;	-----------------------------------------
_isr_SPI:
	iret
;	source/interrupts.c: 20: void isr_TIM1_update(void)	__interrupt (11) /* $8034 11 TIM1 TIM1 update/overflow/underflow/trigger/break */ { }
;	-----------------------------------------
;	 function isr_TIM1_update
;	-----------------------------------------
_isr_TIM1_update:
	iret
;	source/interrupts.c: 21: void isr_TIM1_capture(void)	__interrupt (12) /* $8038 12 TIM1 TIM1 capture/compare */ 	{ }
;	-----------------------------------------
;	 function isr_TIM1_capture
;	-----------------------------------------
_isr_TIM1_capture:
	iret
;	source/interrupts.c: 22: void isr_TIM2_update(void) 	__interrupt (13) /* $803C 13 TIM2 TIM2 update/overflow */ 	{ }
;	-----------------------------------------
;	 function isr_TIM2_update
;	-----------------------------------------
_isr_TIM2_update:
	iret
;	source/interrupts.c: 23: void isr_TIM2_capture(void) 	__interrupt (14) /* $8040 14 TIM2 TIM2 capture/compare */ 	{ }
;	-----------------------------------------
;	 function isr_TIM2_capture
;	-----------------------------------------
_isr_TIM2_capture:
	iret
;	source/interrupts.c: 26: void isr_UART1_tx(void)	__interrupt (17) 	/* $804C 17 UART1 Tx complete */ 		{ }
;	-----------------------------------------
;	 function isr_UART1_tx
;	-----------------------------------------
_isr_UART1_tx:
	iret
;	source/interrupts.c: 27: void isr_UART1_rx(void)	__interrupt (18) 	/* $8050 18 UART1 Receive register DATA FULL */ { }
;	-----------------------------------------
;	 function isr_UART1_rx
;	-----------------------------------------
_isr_UART1_rx:
	iret
;	source/interrupts.c: 28: void isr_I2C(void) 	__interrupt (19) 	/* $8054 19 I2C I2C interrupt */ 		{ }
;	-----------------------------------------
;	 function isr_I2C
;	-----------------------------------------
_isr_I2C:
	iret
;	source/interrupts.c: 31: void isr_ADC1(void) 	__interrupt (22) 	/* $8060 22 ADC1 end of conversion/analog watchdog interrupt */ { }
;	-----------------------------------------
;	 function isr_ADC1
;	-----------------------------------------
_isr_ADC1:
	iret
;	source/interrupts.c: 32: void isr_TIM4(void) 	__interrupt (23) 	/* $8064 23 TIM4 update/overflow */ 		{ }
;	-----------------------------------------
;	 function isr_TIM4
;	-----------------------------------------
_isr_TIM4:
	iret
;	source/interrupts.c: 33: void isr_FLASH(void) 	__interrupt (24) 	/* $8068 24 Flash EOP/WR_PG_DIS */ 		{ }
;	-----------------------------------------
;	 function isr_FLASH
;	-----------------------------------------
_isr_FLASH:
	iret
	.area CODE
	.area INITIALIZER
	.area CABS (ABS)
